var User = require('../model/user-model');
var passport = require('passport');

exports.register = function (req, res) {
    var user = new User(
        {
            name: req.body.name,
            email: req.body.email,
            mobile: req.body.password
        }
    );

    user.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('User Created successfully')
    })
};

exports.login = function (req, res) {
    

    passport.authenticate('local', function(err, user, info){
        var token;
    
        // If Passport throws/catches an error
        if (err) {
          res.status(404).json(err);
          return;
        }
    
        // If a user is found
        if(user){
          token = user.generateJwt();
          res.status(200);
          res.json({
            "token" : token
          });
        } else {
          // If user is not found
          res.status(401).json(info);
        }
      })(req, res);
};

module.exports.profileRead = function(req, res) {

    if (!req.payload._id) {
      res.status(401).json({
        "message" : "UnauthorizedError: private profile"
      });
    } else {
      User
        .findById(req.payload._id)
        .exec(function(err, user) {
          res.status(200).json(user);
        });
    }
};


exports.get_users_details = function (req, res) {
    User.find(req.params.id, { $set: req.body }, function (err, user) {
        if (err) {
            return next(err);
        }


        res.send(user);
    });
};

exports.user_login_list = function (req, res) {
    User.find({}, function (err,user) {
        if (err) return next(err);
        res.send(user);
    })
};

function handleValidationError(err, body) {
    for (field in err.errors) {
        switch (err.errors[field].path) {
            case 'name':
                body['name'] = err.errors[field].message;
                break;
            case 'age':
                body['age'] = err.errors[field].message;
                break;
            case 'email':
                body['email'] = err.errors[field].message;
                break;
            case 'mobile':
                body['mobile'] = err.errors[field].message;
                break;
            case 'address':
                body['address'] = err.errors[field].message;
                break;

            default:
                break;
        }
    }
}