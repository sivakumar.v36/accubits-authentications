var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload',
  algorithms: ['RS256']
});


var user_controller = require('../controller/user');

router.get('/profile', auth, user_controller.profileRead);

router.post('/register', user_controller.register);

router.post('/login', user_controller.login);

router.get('/get_user_details', user_controller.get_users_details);

router.get('/user_login_list', user_controller.user_login_list);


module.exports = router;
